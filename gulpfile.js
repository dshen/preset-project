var gulp = require('gulp');

var browserSync = require('browser-sync').create();
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var csswring = require('csswring');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var autoprefixer = require('autoprefixer-core');
var lost = require('lost');

gulp.task('lint', function(){
  return gulp.src('js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
});

gulp.task('sass', function(){
  var processors = [
      // csswring, //minify css
      lost,
      autoprefixer({browsers:['last 2 versions']})
  ];

  return gulp.src('sass/**/*.scss')
    .pipe(sass())
    .pipe(concat('style.css'))
    .pipe(postcss(processors))
    .pipe(gulp.dest('./dest'))
});

gulp.task('scripts', function(){
  return gulp.src('js/*.js')
  .pipe(concat('all.js'))
  .pipe(gulp.dest('dest'))
  .pipe(rename('all.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('dest'))
});

gulp.task('browserSync', function(){
  browserSync.init({
    server: "."
  });
  gulp.watch("sass/**/*.scss", ['sass']).on('change', browserSync.reload);
  gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('watch', function(){
  gulp.watch('js/*.js', ['lint', 'scripts']);
  gulp.watch('sass/**/*.scss', ['sass']);
});

gulp.task('default', ['lint', 'sass', 'scripts', 'watch', 'browserSync']);

