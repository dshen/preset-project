### This is a preset project include ###
1. normalizer
2. sass, autoprefixer, lost-grid for style
3. jshint for javaScript validation
4. gulp, browserSync

### How to use ###
1. git clone repo
2. Run npm install for node_modules
3. Run gulp